# ivl-deploy

```bash
$ git clone --recursive git@gitlab.com:ivlcityu/ivl-deploy.git
$ cd ivl-deploy
$ git submodule update --recursive --remote
$ docker-compose up -d
```
